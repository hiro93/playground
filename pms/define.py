import typing
from dataclasses import dataclass
from enum import Enum

from pms.document import PMSDocument


class MessageType(Enum):
    """
    消息类型
    """
    CMD = 0
    LOAD = 1
    ECHO = 10

    STEP = 11
    OP = 12
    ICT = 13


class CmdType(Enum):
    """
    指令类型
    """
    AUTO = 0
    STEP = 1
    NEXT = 2
    SKIP = 3
    QUIT = 4


@dataclass
class Message(object):
    type: MessageType
    body: typing.Any


class StatusType(Enum):
    IDLE = 0
    READY = 1
    RUNNING = 2


class ModType(Enum):
    AUTO = 0
    STEP = 1


@dataclass
class PMSState(object):
    """
    运行状态
    """
    mod: ModType = ModType.AUTO
    status: StatusType = StatusType.IDLE
    document: PMSDocument = None
    step: int = 0
    pc: int = 0

    def __init__(self):
        self.ict = []

    def sw_idle(self):
        """
        程序复位
        :return:
        """
        self.status = StatusType.IDLE
        self.pc = 0

    def sw_ready(self, document: PMSDocument):
        """
        ready
        :return:
        """
        self.status = StatusType.READY
        self.document = document
        self.pc = 0

    def sw_running(self):
        self.status = StatusType.RUNNING

    def sw_auto(self):
        self.mod = ModType.AUTO

    def sw_step(self):
        self.mod = ModType.STEP


class PMSException(Exception):
    pass
