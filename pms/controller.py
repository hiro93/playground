import json
import queue
import re
import threading

import peewee as pw
import wx
import xlrd
from docx import Document

from ioss.model import PxiSlotModel, PxiVarModel, HSLVarModel, PMSTimerModel, PMSTableModel


class PMSConfigController(threading.Thread):
    def __init__(self, view):
        threading.Thread.__init__(self)
        self.setDaemon(True)
        self.queue = queue.Queue()
        self.view = view
        self.__is_running = False

    def parse(self, path):
        self.queue.put({
            'task': 'parse',
            'path': path
        })

    def parse_handbook(self, path):
        self.queue.put({
            'task': 'parse_handbook',
            'path': path
        })

    def run(self):
        while self.__is_running:
            task = self.queue.get()
            if not task:
                continue
            if task['task'] == 'parse':
                self.__on_parse(task['path'])
            elif task['task'] == 'parse_handbook':
                self.__on_parse_handbook(task['path'])

    def stop(self):
        self.__is_running = False
        self.queue.put_nowait(0)
        self.join()

    def start(self):
        self.__is_running = True
        super(PMSConfigController, self).start()

    def __on_parse(self, path):
        self.view.btn_ok.Enable(False)
        self.view.EnableCloseButton(False)
        try:
            book: xlrd.Book = xlrd.open_workbook(path)
            self.__log_view(f'open file {path}, {book}')
            self.__parse_slot(book.sheet_by_name('SLOT'))
            self.__parse_pxi(book.sheet_by_name('PXI'))
            self.__parse_hsl(book.sheet_by_name('HSL'))
            self.__parse_timer(book.sheet_by_name('TIMER'))
        except Exception as e:
            self.__log_view(e)
        finally:
            self.view.EnableCloseButton(True)
            self.view.btn_ok.Enable(True)

    def __on_parse_handbook(self, path):
        self.view.btn_ok.Enable(False)
        self.view.EnableCloseButton(False)
        try:
            document = Document(path)
            self.__parse_handbook(document)
        except Exception as e:
            self.__log_view(e)
        finally:
            self.view.EnableCloseButton(True)
            self.view.btn_ok.Enable(True)

    def __parse_slot(self, sheet: xlrd.sheet.Sheet):
        """
        读取并解析 PxiSlot 配置表
        :param sheet:
        :return:
        """
        fields = ['slot', 'protocol', 'ip', 'port', 'slave_id', 'desc']
        self.__log_view('读取 SLOT 配置表')
        head = sheet.row_values(0)
        if head != fields:
            raise ValueError(f'表格格式错误')

        data = []
        for cx in range(1, sheet.nrows):
            (slot,
             protocol,
             ip, port, slave_id,
             desc) = sheet.row_values(cx)
            data.append(PxiSlotModel(slot=slot, protocol=protocol, ip=ip, port=port, slave_id=slave_id, desc=desc))
            self.__log_view(f'{sheet.row_values(cx)}')

        db: pw.Database = PxiSlotModel._meta.database
        with db.atomic():
            self.__log_view(f'clear all PxiSlot')
            PxiSlotModel.delete().execute()
            for batch in pw.chunked(data, n=20):
                PxiSlotModel.bulk_create(batch)
            self.__log_view(f'bulk create PxiSlot')

    def __parse_pxi(self, sheet: xlrd.sheet.Sheet):
        """
        读取并解析 PxiVar 表
        :param sheet:
        :return:
        """
        fields = ['id', 'sig_type', 'sig_name', 'slot', 'channel',
                  'engineering_unit', 'rlo', 'rhi', 'elo', 'ehi',
                  'initial', 'reg', 'chr', 'block', 'offset', 'bit']

        self.__log_view(f'读取 PXI 配置表')
        head = sheet.row_values(0)
        if head != fields:
            raise ValueError(f'表格格式错误')

        data = []
        for cx in range(1, sheet.nrows):
            (id, sig_type, sig_name,
             slot, channel, engineering_unit,
             rlo, rhi, elo, ehi,
             initial, reg, chr, block, offset, bit) = sheet.row_values(cx)
            rlo = float(rlo) if rlo is not '' else None
            rhi = float(rhi) if rhi is not '' else None
            elo = float(elo) if elo is not '' else None
            ehi = float(ehi) if ehi is not '' else None
            initial = float(initial) if initial is not '' else None
            data.append(PxiVarModel(id=int(id), sig_type=sig_type, sig_name=sig_name,
                                    slot=slot, channel=channel, engineering_unit=engineering_unit,
                                    rlo=rlo, rhi=rhi, elo=elo, ehi=ehi,
                                    initial=initial, reg=reg, chr=chr, block=block, offset=offset, bit=bit))
            self.__log_view(f'{sheet.row_values(cx)}')

        db: pw.Database = PxiVarModel._meta.database
        with db.atomic():
            self.__log_view(f'clear all PxiVar')
            PxiVarModel.delete().execute()
            for batch in pw.chunked(data, n=60):
                PxiVarModel.bulk_create(batch)
            self.__log_view(f'bulk create PxiVar')

    def __parse_hsl(self, sheet: xlrd.sheet.Sheet):
        """
        读取并解析 HSL 配置表
        :param sheet:
        :return:
        """
        fields = ['id', 'sig_type', 'sig_name', 'port', 'index', 'data_type', 'bit',
                  'engineering_unit', 'rlo', 'rhi', 'elo', 'ehi', 'initial']

        self.__log_view(f'读取 PXI 配置表')
        head = sheet.row_values(0)
        if head != fields:
            raise ValueError(f'表格格式错误')

        data = []
        for cx in range(1, sheet.nrows):
            (id, sig_type, sig_name, port, index, data_type, bit,
             engineering_unit, rlo, rhi, elo, ehi, initial) = sheet.row_values(cx)
            data.append(HSLVarModel(id=id, sig_type=sig_type, sig_name=sig_name, port=port,
                                    index=index, data_type=data_type, bit=bit,
                                    engineering_unit=engineering_unit,
                                    rlo=rlo, rhi=rhi, elo=elo, ehi=ehi, initial=initial))
            self.__log_view(f'{sheet.row_values(cx)}')

        db: pw.Database = HSLVarModel._meta.database
        with db.atomic():
            self.__log_view(f'clear all HSLVar')
            HSLVarModel.delete().execute()
            for batch in pw.chunked(data, n=60):
                HSLVarModel.bulk_create(batch)
            self.__log_view(f'bulk create HSLVar')

    def __parse_timer(self, sheet: xlrd.sheet.Sheet):
        """
        解析并读取 PMSTimer 表
        :param sheet:
        :return:
        """
        fields = ['timer_system_id', 'pms_slot', 'sig_name']
        self.__log_view('读取 PMSTimer 配置表')
        head = sheet.row_values(0)
        if head != fields:
            raise ValueError(f'表格格式错误')

        data = []
        for cx in range(1, sheet.nrows):
            (timer_system_id, pms_slot, sig_name) = sheet.row_values(cx)
            data.append(PMSTimerModel(timer_system_id=timer_system_id, pms_slot=pms_slot, sig_name=sig_name))
            self.__log_view(f'{sheet.row_values(cx)}')

        db: pw.Database = PMSTimerModel._meta.database
        with db.atomic():
            self.__log_view(f'clear all PMSTimer')
            PMSTimerModel.delete().execute()
            for batch in pw.chunked(data, n=20):
                PMSTimerModel.bulk_create(batch)
            self.__log_view(f'bulk create PMSTimer')

    def __parse_handbook(self, document):
        par_2 = re.compile('(?:TR-RT|XOTR|TR-ESF|XITR)[-_a-zA-Z0-9]{1,}')

        # data = []
        for table in document.tables:
            if table.cell(0, 0).text == 'Step':
                script_name = ','.join(par_2.findall(table.cell(1, 1).text))
                timer_system_id = '{},{},{},{}'.format(
                    table.cell(2, 2).text,
                    table.cell(2, 3).text,
                    table.cell(2, 4).text,
                    table.cell(2, 5).text,
                )

                # 针对下划线 将带下划线的< 号转为 ≤
                required_time = re.findall(r'\d+', table.cell(3, 0).text)[0]
                content = {
                    'step': table.cell(1, 0).text,
                    'script': table.cell(1, 1).text,
                    'table': table.cell(1, 5).text,
                    'function': table.cell(1, 6).text,
                    'specification': table.cell(1, 7).text,

                    'req_1': table.cell(2, 2).text,
                    'req_2': table.cell(2, 3).text,
                    'req_3': table.cell(2, 5).text,
                    'req_4': table.cell(2, 7).text,

                    'required_time': required_time,
                    'req_1_v': table.cell(3, 2).text,
                    'req_2_v': table.cell(3, 3).text,
                    'req_3_v': table.cell(3, 5).text,
                    'req_4_v': table.cell(3, 7).text,
                    'sat_circle': table.cell(3, 8).text,
                    'date': table.cell(3, 9).text,
                    'initial': table.cell(3, 10).text,
                }
                table_type = 1
            else:
                script_name = ','.join(par_2.findall(table.cell(0, 0).text))
                if len(table.rows[1].cells) == 6:
                    content = {
                        'script': table.cell(0, 0).text,
                        'value': []
                    }
                    for row in table.rows[2:]:
                        val_li = [cell.text for cell in row.cells]
                        try:
                            val_li[1] = re.findall(r'\d+', val_li[1])[0]
                        except Exception:
                            pass
                        content['value'].append(val_li)

                    table_type = 2
                else:
                    content = {
                        'script': table.cell(0, 0).text,
                        'value': [],
                    }
                    for row in table.rows[2:]:
                        val_li = [cell.text for cell in row.cells]
                        try:
                            val_li[1] = re.findall(r'\d+', val_li[1])[0]
                        except Exception:
                            pass
                        content['value'].append(val_li)

                    if 'Incoming' in table.cell(1, 2).text:
                        content['merge_value'] = table.cell(2, 3).text
                        table_type = 3
                    else:
                        content['merge_value'] = table.cell(2, 2).text
                        table_type = 4
                timer_system_id = ','.join([val[0] for val in content['value']])
            self.__log_view(f'Table {table_type}, script={script_name}')
            content = json.dumps(content)
            obj, created = PMSTableModel.get_or_create(
                script_name=script_name,
                defaults={
                    'timer_system_id': timer_system_id,
                    'content': content,
                    'table_type': table_type
                }
            )
            if not created:
                obj.timer_system_id = timer_system_id
                obj.content = content
                obj.table_type = table_type
                obj.save()
        #     data.append({
        #         'timer_system_id': timer_system_id,
        #         'script_name': script_name,
        #         'content': content,
        #         'table_type': table_type
        #     })
        # return data

    def __log_view(self, e):
        def callback():
            self.view.text.AppendText(f'{e}\n')

        wx.CallAfter(callback)
