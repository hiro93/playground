import logging
from functools import partial
from queue import Queue

import wx
import wx.grid as GRID

from pms.controller import PMSConfigController
from pms.define import Message, CmdType, MessageType, ModType
from pms.document import PMSDocument
from pms.worker import WorkingThread


class DocumentGrid(wx.Panel):
    __document: PMSDocument

    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        self.__logger = logging.getLogger('pms.view')
        self.__layout()

        self.__queue = Queue()
        self.__worker = WorkingThread(self.__queue, self.callback)
        self.__worker.setDaemon(True)
        self.__worker.start()

    def __layout(self):
        # self.SetBackgroundColour(wx.WHITE)
        self.btnPanel = wx.Panel(self)
        self.btnF1 = wx.Button(self.btnPanel, wx.ID_FILE1, 'Auto')
        self.btnF2 = wx.Button(self.btnPanel, wx.ID_FILE2, 'Step')
        self.btnF3 = wx.Button(self.btnPanel, wx.ID_FILE3, 'Next')
        self.btnF4 = wx.Button(self.btnPanel, wx.ID_FILE4, 'Skip')
        self.btnF5 = wx.Button(self.btnPanel, wx.ID_FILE5, 'Quit')
        self.btnF6 = wx.Button(self.btnPanel, wx.ID_FILE6, 'Load')
        self.btnF7 = wx.Button(self.btnPanel, wx.ID_FILE7, 'Conf')

        self.disPanel = wx.Panel(self)
        self.grid = GRID.Grid(self.disPanel)
        self.grid.CreateGrid(20, 3)
        self.grid.HideRowLabels()
        self.logCtrl = wx.TextCtrl(self.disPanel, style=wx.TE_MULTILINE | wx.TE_AUTO_URL, size=(-1, 120))
        dsz = wx.BoxSizer(wx.VERTICAL)
        dsz.Add(self.grid, 1, wx.EXPAND | wx.ALL ^ wx.LEFT, 5)
        dsz.Add(self.logCtrl, 0, wx.EXPAND | wx.ALL ^ wx.LEFT ^ wx.TOP, 5)
        self.disPanel.SetSizer(dsz)
        self.disPanel.Layout()

        bsz = wx.BoxSizer(wx.VERTICAL)
        bsz.Add(self.btnF1, 0, wx.EXPAND | wx.ALL, 5)
        bsz.Add(self.btnF2, 0, wx.EXPAND | wx.ALL ^ wx.TOP, 5)
        bsz.Add(wx.StaticLine(self.btnPanel), 0, wx.EXPAND | wx.ALL ^ wx.TOP, 5)
        bsz.Add(self.btnF3, 0, wx.EXPAND | wx.ALL ^ wx.TOP, 5)
        bsz.Add(self.btnF4, 0, wx.EXPAND | wx.ALL ^ wx.TOP, 5)
        bsz.AddStretchSpacer()
        bsz.Add(self.btnF5, 0, wx.EXPAND | wx.ALL ^ wx.TOP, 5)
        bsz.Add(self.btnF6, 0, wx.EXPAND | wx.ALL ^ wx.TOP, 5)
        bsz.Add(self.btnF7, 0, wx.EXPAND | wx.ALL ^ wx.TOP, 5)
        self.btnPanel.SetSizer(bsz)
        self.btnPanel.Layout()

        msz = wx.BoxSizer(wx.HORIZONTAL)
        msz.Add(self.btnPanel, 0, wx.EXPAND)
        msz.Add(self.disPanel, 1, wx.EXPAND)
        self.SetSizer(msz)
        self.Layout()

        self.Bind(wx.EVT_BUTTON, partial(self.__on_cmd, cmd=CmdType.AUTO), id=wx.ID_FILE1)
        self.Bind(wx.EVT_BUTTON, partial(self.__on_cmd, cmd=CmdType.STEP), id=wx.ID_FILE2)
        self.Bind(wx.EVT_BUTTON, partial(self.__on_cmd, cmd=CmdType.NEXT), id=wx.ID_FILE3)
        self.Bind(wx.EVT_BUTTON, partial(self.__on_cmd, cmd=CmdType.SKIP), id=wx.ID_FILE4)
        self.Bind(wx.EVT_BUTTON, partial(self.__on_cmd, cmd=CmdType.QUIT), id=wx.ID_FILE5)
        self.Bind(wx.EVT_BUTTON, self.__on_load, id=wx.ID_FILE6)
        self.Bind(wx.EVT_BUTTON, self.__on_conf, id=wx.ID_FILE7)
        self.Bind(wx.EVT_CONTEXT_MENU, self.__on_context, source=self.btnPanel)

    def __on_cmd(self, evt: wx.CommandEvent, cmd=CmdType.AUTO):
        self.__queue.put(Message(MessageType.CMD, cmd))
        evt.Skip()

    def __on_context(self, evt):
        menu = wx.Menu()
        menu.Append(wx.ID_SETUP, '设置')
        _id = self.GetPopupMenuSelectionFromUser(menu)
        if _id == wx.ID_SETUP:
            from ioss.view import IOssView
            dlg = wx.Dialog(self.GetTopLevelParent(), -1, 'IOss Config', size=(600, 400))
            pan = IOssView(dlg)
            for slot in PxiSlotModel.select():
                pan.append(slot)

            msz = wx.BoxSizer()
            msz.Add(pan, 1, wx.EXPAND)
            dlg.SetSizer(msz)
            dlg.Show()
            # dlg.ShowModal()
            # dlg.Destroy()
        evt.Skip()

    def __on_load(self, evt: wx.CommandEvent):
        with wx.FileDialog(self.GetTopLevelParent()) as dlg:
            if dlg.ShowModal() == wx.ID_OK:
                self.__queue.put(Message(MessageType.LOAD, PMSDocument(dlg.GetPath())))
        evt.Skip()

    def __on_conf(self, evt: wx.CommandEvent):
        with PMSConfigDialog(self.GetTopLevelParent()) as dlg:
            if dlg.ShowModal() == wx.ID_OK:
                pass
        evt.Skip()

    def __do_echo(self, message: Message):
        if message.type == MessageType.LOAD:
            document: PMSDocument = message.body
            text = f'打开脚本: {document.name}\n'
            self.logCtrl.AppendText(text)
            self.render(message.body)
        elif message.type == MessageType.CMD:
            if self.__worker.state.mod == ModType.AUTO:
                self.btnF1.Enable(False)
                self.btnF2.Enable(True)
                self.btnF3.Enable(False)
                self.btnF4.Enable(True)
                self.btnF5.Enable(True)
            elif self.__worker.state.mod == ModType.STEP:
                self.btnF1.Enable(True)
                self.btnF2.Enable(False)
                self.btnF3.Enable(True)
                self.btnF4.Enable(True)
                self.btnF5.Enable(True)
            text = f'命令：{message.body}\n'
            start = self.logCtrl.GetLastPosition()
            self.logCtrl.AppendText(text)
            end = self.logCtrl.GetLastPosition()
            attr = wx.TextAttr()
            attr.SetTextColour(wx.RED)
            self.logCtrl.SetStyle(start, end, style=attr)
        elif message.type == MessageType.STEP:
            cx, rx, bg = message.body
            self.grid.SetCellBackgroundColour(rx, cx, bg)
            self.grid.RefreshAttr(rx, cx)
            self.grid.Refresh()
            # text = f'运行第 {message.body + 1} 步\n'
            # self.logCtrl.AppendText(text)

    def callback(self, message: Message):
        # self.__logger.debug(f'receive message {message}')
        if message.type == MessageType.ECHO:
            wx.CallAfter(self.__do_echo, message.body)

    def render(self, document: PMSDocument):
        n_cols = len(document.cols())
        n_rows = len(document.rows()) - 1
        g_cols = self.grid.GetNumberCols()
        g_rows = self.grid.GetNumberRows()
        self.grid.ClearGrid()
        if g_cols < n_cols:
            self.grid.AppendCols(n_cols - g_cols)
        if g_rows < n_rows:
            self.grid.AppendRows(n_rows - g_rows)
        rows = document.rows()
        for cx, cell in enumerate(rows[0]):
            self.grid.SetColLabelValue(cx, cell.text)

        for rx, row in enumerate(rows[1:]):
            self.grid.BeginBatch()
            for cx, cell in enumerate(row):
                self.grid.SetCellValue(rx, cx, cell.text)
                self.grid.SetCellTextColour(rx, cx, cell.fg_color)
                self.grid.SetCellBackgroundColour(rx, cx, cell.bg_color)
                if cell.type == 'head':
                    font = wx.Font(wx.FONTSIZE_SMALL, wx.FONTFAMILY_SCRIPT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD)
                    self.grid.SetCellFont(rx, cx, font)
                    self.grid.SetCellEditor(rx, cx, GRID.GridCellAutoWrapStringEditor())
            self.grid.EndBatch()
            wx.YieldIfNeeded()
        self.grid.AutoSizeColumns()


class PMSConfigDialog(wx.Dialog):
    def __init__(self, parent):
        wx.Dialog.__init__(self, parent, -1, 'PMS Config', size=(600, 400))
        self.__layout()
        self.controller = PMSConfigController(self)
        self.controller.start()

    def wildcard(self):
        if self.choice.GetSelection() == 0:
            return 'PMS Config(*.xlsx)|*.xlsx'
        else:
            return 'Result Handbook(*.docx)|*.docx'

    def file_type(self):
        if self.choice.GetSelection() == 0:
            return 'PMS'
        else:
            return 'HANDBOOK'

    def __layout(self):
        self.choice = wx.Choice(self, choices=['PMS', 'Result'])
        self.choice.SetSelection(0)
        self.picker = wx.FilePickerCtrl(self, wildcard=self.wildcard())
        self.text = wx.TextCtrl(self, style=wx.TE_MULTILINE | wx.TE_READONLY)
        self.btn_ok = wx.Button(self, id=wx.ID_OK)
        self.psz = wx.BoxSizer(wx.HORIZONTAL)
        self.psz.Add(wx.StaticText(self, label='配置文件'), 0, wx.ALL | wx.ALIGN_CENTER, 10)
        self.psz.Add(self.choice, 0, wx.ALL ^ wx.LEFT, 10)
        self.psz.Add(self.picker, 1, wx.EXPAND | wx.ALL ^ wx.LEFT, 10)

        msz = wx.BoxSizer(wx.VERTICAL)
        msz.Add(self.psz, 0, wx.EXPAND)
        msz.Add(self.text, 1, wx.EXPAND | wx.ALL ^ wx.TOP, 10)
        msz.Add(self.btn_ok, 0, wx.ALIGN_CENTER | wx.ALL ^ wx.TOP, 10)
        self.SetSizer(msz)
        self.Center()

        self.text.AppendText(f'*** 注意：导入 PMS 通信配置文件将导致通信中断，请确保没有正在执行脚本 ***\n')
        self.btn_ok.Enable(False)
        self.Bind(wx.EVT_FILEPICKER_CHANGED, self.__on_picker)
        self.Bind(wx.EVT_CHOICE, self.__on_choice, source=self.choice)

    def __on_choice(self, evt):
        self.picker.Destroy()
        self.picker = wx.FilePickerCtrl(self, wildcard=self.wildcard())
        self.psz.Add(self.picker, 1, wx.EXPAND | wx.ALL ^ wx.LEFT, 10)
        self.Layout()
        evt.Skip()

    def __on_picker(self, evt: wx.FileDirPickerEvent):
        if self.file_type() == 'PMS':
            self.controller.parse(evt.GetPath())
        else:
            self.controller.parse_handbook(evt.GetPath())
        evt.Skip()

    def Destroy(self):
        self.controller.stop()
        super(PMSConfigDialog, self).Destroy()


if __name__ == '__main__':
    from pathlib import Path
    from ioss.model import PxiSlotModel
    from ioss.table import ioss


    class Frame(wx.Frame):
        def __init__(self):
            wx.Frame.__init__(self, None, -1, 'PMS', size=(900, 600))
            self.panel = DocumentGrid(self)
            sizer = wx.BoxSizer()
            sizer.Add(self.panel, 1, wx.EXPAND)
            self.SetSizer(sizer)
            self.Layout()


    logging.basicConfig(level=logging.DEBUG)
    ioss.setup(Path('../static/pms'))
    # init_db(pw.SqliteDatabase(Path('../static/pms/pms.db')))
    app = wx.App()
    frame = Frame()
    frame.Show()
    app.SetTopWindow(frame)
    app.MainLoop()
