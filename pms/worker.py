import logging
import threading
import time
import typing
from logging import Logger
from queue import Queue

from ioss.table import ioss
from pms.define import (Message, MessageType, CmdType, PMSState, StatusType, ModType)
from pms.document import PMSDocument


class WorkingThread(threading.Thread):
    """
    工作线程，接收外部命令，完成脚本运行工作
    """
    state: PMSState
    __callback: typing.Callable
    __logger: Logger
    __queue: Queue

    def __init__(self, q, callback, logger=None):
        threading.Thread.__init__(self)
        self.__queue = q
        self.__logger = logger or logging.getLogger('pms.worker')
        self.__callback = callback
        self.__is_running = False
        self.state = PMSState()

    def run(self):
        # ioss.write('WD', 0)
        # ioss.write('TWD', 0)
        # ioss.write('TRD', 0)
        # ioss.write('TTD', 0)
        # ioss.write('TSD', 0)

        while self.__is_running:
            message: Message = self.__queue.get()
            self.__logger.debug(f'Receive message {message}')

            # 指令消息事件
            if message.type == MessageType.CMD:
                self.__on_cmd(message)
            elif message.type == MessageType.LOAD:
                self.__on_load(message)
            elif message.type == MessageType.OP:
                self.__on_op(message)
            elif message.type == MessageType.ICT:
                self.__on_ict(message)

    def start(self):
        self.__is_running = True
        super(WorkingThread, self).start()

    def __on_cmd(self, message: Message) -> None:
        assert message.type == MessageType.CMD and isinstance(message.body, CmdType)
        cmd: CmdType = message.body
        if cmd == CmdType.AUTO:
            # 响应 AUTO 自动执行指令
            if self.state.status == StatusType.IDLE:
                self.__logger.error(f'load document first')
            elif self.state.status == StatusType.READY:
                self.state.sw_idle()
                self.state.sw_auto()
                self.state.sw_running()
                self.__logger.info(f'START RUNNING')
                self.__callback(Message(MessageType.ECHO, message))
                self.__queue.put(Message(MessageType.OP, self.state.pc))
            elif self.state.status == StatusType.RUNNING:
                self.state.sw_auto()
        elif cmd == CmdType.STEP:
            # 响应 STEP 单步执行指令
            if self.state.status == StatusType.IDLE:
                self.__logger.error(f'load document first')
            elif self.state.status == StatusType.READY:
                self.state.sw_idle()
                self.state.sw_step()
                self.state.sw_running()
                self.__logger.info(f'START RUNNING')
                self.__callback(Message(MessageType.ECHO, message))
                self.__queue.put(Message(MessageType.OP, self.state.pc))
            elif self.state.status == StatusType.RUNNING:
                self.state.sw_step()
        elif cmd == CmdType.NEXT:
            # 响应 NEXT 指令
            if self.state.status == StatusType.IDLE:
                self.__logger.error(f'load document first')
            elif self.state.status == StatusType.READY:
                self.__logger.error(f'START RUNNING first')
            elif self.state.status == StatusType.RUNNING:
                if self.state.mod == ModType.AUTO:
                    self.__logger.warning(f'ignore NEXT in AUTO mod')
                elif self.state.mod == ModType.STEP:
                    self.__pc_plus()
        elif cmd == CmdType.SKIP:
            pass
        elif cmd == CmdType.QUIT:
            pass

    def __on_load(self, message: Message):
        assert message.type == MessageType.LOAD and isinstance(message.body, PMSDocument)
        document: PMSDocument = message.body
        document.parse()
        self.state.sw_ready(document)
        self.__callback(Message(MessageType.ECHO, message))

    def __on_op(self, message: Message):
        assert message.type == MessageType.OP
        cols = self.state.document.cols()

        def callback():
            self.__logger.debug(f'RUN OP pc={self.state.pc}, cx={cx + 1}, rx={rx}, CELL={cell}, {col}')

            head = cols[0][1:]
            attr = head[rx]
            if rx == 0:
                # step 之前的初始化工作
                try:
                    self.__callback(Message(MessageType.ECHO, Message(MessageType.STEP, (cx + 1, rx, 'yellow'))))
                    self.__logger.info(f'START STEP {cx} Comments={cell.text}')
                except Exception as e:
                    self.__logger.error(e)
                    self.__callback(Message(MessageType.ECHO, Message(MessageType.STEP, (cx + 1, rx, 'green'))))
                return self.__pc_plus()
            elif rx == 1:
                # input capture time
                self.__callback(Message(MessageType.ECHO, Message(MessageType.STEP, (cx + 1, rx, 'yellow'))))
                self.__logger.debug(f'Input Capture Times={cell.text}')
                self.state.ict.append(cell.text)
                self.__callback(Message(MessageType.ECHO, Message(MessageType.STEP, (cx + 1, rx, 'green'))))
                return self.__pc_plus()
            else:
                self.__callback(Message(MessageType.ECHO, Message(MessageType.STEP, (cx + 1, rx, 'yellow'))))
                sig_name, value = attr.text, cell.text
                self.__logger.debug(f'SET {sig_name}={value}')
                try:
                    ioss.write(sig_name, value)
                    self.__callback(Message(MessageType.ECHO, Message(MessageType.STEP, (cx + 1, rx, 'green'))))
                except (IOError, ConnectionRefusedError) as e:
                    self.__logger.error(e)
                    self.__callback(Message(MessageType.ECHO, Message(MessageType.STEP, (cx + 1, rx, 'red'))))

                # step 运行完毕，清理工作
                if rx == len(col) - 2:
                    ioss.write('TTD', 0)
                    # ioss.write('TSD', 1)
                    ioss.write('WD', ioss.read('WD') + 1)
                    self.__logger.info(f'FINISH STEP {cx}')
                    self.__queue.put(Message(MessageType.ICT, self.state.ict.pop()))
                    if self.state.mod == ModType.AUTO:
                        return self.__pc_plus()
                    else:
                        return
                return self.__pc_plus()

        idx = 0
        for cx, col in enumerate(cols[1:]):
            for rx, cell in enumerate(col[1:]):
                if self.state.pc == idx:
                    return callback()
                idx += 1

    def __on_ict(self, message: Message):
        assert message.type == MessageType.ICT
        self.__logger.info(f'WAITING {message.body}')
        time.sleep(1)

    def __pc_plus(self):
        # 执行下一条指令
        self.state.pc += 1
        self.__queue.put(Message(MessageType.OP, self.state.pc))
