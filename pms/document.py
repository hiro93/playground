import csv
import typing
from dataclasses import dataclass
from pathlib import Path


class PMSDocument(object):
    path: Path

    def __init__(self, path):
        self.name = None
        self.path = Path(path)
        self.raw_data = []
        self.__rows = []
        self.__cols = []

    def parse(self):
        with self.path.open('r') as fp:
            reader = csv.reader(fp, delimiter='\t')
            rows = [x for x in reader if x]

        self.name = self.path.with_suffix('').name

        self.raw_data.clear()
        self.raw_data.extend(rows)
        self.__rows.clear()
        self.__cols.clear()
        self.rows()
        self.cols()

    def rows(self):
        if not self.__rows:
            cols = self.cols()
            n_cols = len(cols)
            n_rows = len(cols[0])
            rows = [[] for x in range(n_rows)]
            for rx in range(n_rows):
                for cx in range(n_cols):
                    rows[rx].append(cols[cx][rx])
            self.__rows.extend(rows)
        return self.__rows

    def cols(self):
        if not self.__cols:
            n_cols = max(len(x) for x in self.raw_data)
            cols = []
            for cx in range(n_cols):
                cols.append([])
                for rx, row in enumerate(self.raw_data):
                    if cx >= len(row):
                        text = ''
                    else:
                        text = row[cx]
                    if cx == 0:
                        _type = 'head'
                    elif rx < 3:
                        _type = 'head'
                    else:
                        _type = 'data'

                    c = _Cell(_type, text)
                    cols[cx].append(c)
            self.__cols.extend(cols)
        return self.__cols

    def block(self, idx):
        """
        block 是一个执行步骤step
        :param idx:
        :return:
        """
        assert 0 <= idx < len(self.rows())
        cols = self.cols()
        head = cols[0]
        cols = cols[idx + 1]
        block = _Block(steps=cols[0].text, comments=cols[1].text, ict=cols[2].text, ops=[])
        for idx in range(3, len(head)):
            block.ops.append(_OP('W', head[idx].text, cols[idx].text))
        return block


@dataclass
class _Cell:
    type: str = 'data'
    text: str = ''
    value: str = ''
    bg_color: str = 'white'
    fg_color: str = 'black'


@dataclass
class _Block:
    steps: str
    comments: str
    ict: str
    ops: list


@dataclass
class _OP:
    mod: str
    sig_name: str
    value: str


@dataclass
class RunStep(object):
    idx: int  # step index
    comment: str  # Comments
    ict: str  # Input Capture Times

    ops: typing.List[_OP]  # operations
    op_idx: int = 0

    def is_trip(self):
        """
        计时开始步骤
        :return:
        """
        for op in self.ops:
            if op.sig_name.startswith('START_PXI') and op.value == '1':
                return True
