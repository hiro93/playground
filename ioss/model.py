import logging

import peewee as pw


class BaseModel(pw.Model):
    class Meta:
        database = pw.Proxy()


class PxiVarModel(BaseModel):
    id = pw.AutoField()
    sig_type = pw.CharField()
    sig_name = pw.CharField()
    slot = pw.CharField()
    channel = pw.CharField()
    engineering_unit = pw.CharField()
    rlo = pw.FloatField(null=True)
    rhi = pw.FloatField(null=True)
    elo = pw.FloatField(null=True)
    ehi = pw.FloatField(null=True)
    initial = pw.FloatField(null=True)
    reg = pw.IntegerField()
    chr = pw.CharField()
    block = pw.IntegerField()
    offset = pw.IntegerField()
    bit = pw.IntegerField()

    class Meta:
        table_name = 't_pxi_var'


class HSLVarModel(BaseModel):
    id = pw.AutoField()
    sig_type = pw.CharField()
    sig_name = pw.CharField()
    port = pw.IntegerField()
    index = pw.IntegerField()
    data_type = pw.CharField()
    bit = pw.IntegerField()
    engineering_unit = pw.CharField()
    rlo = pw.FloatField()
    rhi = pw.FloatField()
    elo = pw.FloatField()
    ehi = pw.FloatField()
    initial = pw.FloatField()

    class Meta:
        table_name = 't_hsl_var'

    @property
    def channel(self):
        return f'P{self.port}|IDX{self.index}:{self.bit}'


class PxiSlotModel(BaseModel):
    id = pw.AutoField()
    slot = pw.CharField()
    protocol = pw.CharField()
    ip = pw.CharField()
    port = pw.IntegerField()
    slave_id = pw.IntegerField(default=1)
    desc = pw.CharField()

    class Meta:
        table_name = 't_pxi_slot'


class PMSTimerModel(BaseModel):
    timer_system_id = pw.CharField()
    pms_slot = pw.CharField()
    sig_name = pw.CharField()

    class Meta:
        table_name = 't_pms_timer'


class PMSTableModel(BaseModel):
    timer_system_id = pw.TextField()
    script_name = pw.CharField()
    content = pw.TextField()
    table_type = pw.IntegerField()

    class Meta:
        table_name = 't_pms_table'

    @classmethod
    def get_by_xhid(cls, timer_system_id):
        return cls.select().where(cls.timer_system_id.contains(timer_system_id))

    @classmethod
    def get_by_scname(cls, script_name):
        return cls.select().where(cls.script_name.contains(script_name))


def init_db(database: pw.SqliteDatabase):
    # TODO: disable peewee logger
    pw.logger.setLevel(logging.INFO)
    proxy: pw.Proxy = BaseModel._meta.database
    proxy.initialize(database)

    PxiSlotModel.create_table(safe=True)
    PxiVarModel.create_table(safe=True)
    HSLVarModel.create_table(safe=True)
    PMSTimerModel.create_table(safe=True)
    PMSTableModel.create_table(safe=True)
