import logging
import logging.handlers
from pathlib import Path

import peewee as pw
from modbus_tk.exceptions import ModbusError

from ioss.define import Var, VarNotFound, PxiVar, HSLVar, AliasVar
from ioss.device import PxiHSL, PxiMaster
from ioss.model import init_db, PxiVarModel, HSLVarModel, PMSTimerModel, PxiSlotModel
from ioss.mormory import MemoryTable

_alias = {
    'START_PXI4': 'TSD'
}


class VarTable(object):
    """
    变量表
    """
    def find(self, sig_name) -> Var:
        if sig_name in _alias:
            alias = _alias[sig_name]
            var = self.find(alias)
            var = AliasVar(
                sig_name=sig_name,
                sig_type=var.sig_type,
                block=var.block,
                var=var,
                desc=f'{sig_name} -> {var.sig_name}'
            )
            return var
        obj = PxiVarModel.filter(PxiVarModel.sig_name == sig_name).first()
        if obj:
            var = PxiVar.from_obj(obj)
            return var

        obj = HSLVarModel.filter(HSLVarModel.sig_name == sig_name).first()
        if obj:
            var = HSLVar.from_obj(obj)
            return var

        obj = PMSTimerModel.filter(PMSTimerModel.sig_name == sig_name).first()
        if obj:
            p_obj = PxiVarModel \
                .filter(PxiVarModel.sig_name == obj.sig_name) \
                .filter(PxiVarModel.sig_type == 'sTO') \
                .first()
            if not p_obj:
                raise VarNotFound(obj.sig_name)
            var = PxiVar.from_obj(p_obj)
            var = AliasVar(
                sig_name=sig_name,
                sig_type=var.sig_type,
                block=var.block,
                var=var,
                desc=obj.pms_slot
            )
            return var

        raise VarNotFound(sig_name)


class IOSS(object):
    table: VarTable
    memory: MemoryTable
    path: Path

    def __init__(self, path=None):
        self.path = path
        self.database = None
        self.devices = {}
        self.table = None
        self.memory = None
        self.__logger = logging.getLogger('ioss')

    def setup(self, path: Path):
        if not path.exists():
            path.mkdir(parents=True)

        # 工作目录
        self.path = path

        # 数据库
        self.database = pw.SqliteDatabase(path.joinpath('ioss.db'))
        init_db(self.database)

        self.memory = MemoryTable()
        self.table = VarTable()

        # 日志
        p_log = self.path.joinpath('log')
        if not p_log.exists():
            p_log.mkdir()
        hdlr = logging.handlers.TimedRotatingFileHandler(filename=p_log.joinpath('io.log'))
        fmt = '%(asctime)15s - %(name)s - %(levelname)s - [%(module)s:%(lineno)d] - %(message)s'
        hdlr.setFormatter(logging.Formatter(fmt))
        self.__logger.addHandler(hdlr)
        self.__logger.debug('Hello World')

        for obj in PxiSlotModel.select():
            self.register(obj)

    def register(self, obj: PxiSlotModel):
        if obj.slot in self.devices:
            dev = self.devices[obj.slot]
            dev.setup(obj)
            return dev

        if obj.slot == 'PXI_HSL':
            dev = PxiHSL(obj.ip, obj.port)
        else:
            dev = PxiMaster(obj.ip, obj.port)
        self.devices[obj.slot] = dev
        return dev

    def write(self, sig_name, value, remote=True):
        """
        写变量
        :param sig_name: 信号名
        :param value: 值
        :param remote: 写入到外部接口，通常会阻塞
        :return:
        """
        var = self.table.find(sig_name)
        if remote:
            try:
                self.__write(var, value)
            except ModbusError as e:
                raise IOError(e)
        self.memory.write(var, value)
        return value

    def read(self, sig_name, remote=True):
        """
        读变量
        :param sig_name: 信号名
        :param remote: 从外部接口读，通常会阻塞
        :return:
        """
        var = self.table.find(sig_name)
        if remote:
            value = self.__read(var)
            self.memory.write(var, value)
            return self.memory.read(var)
            # return value
        return self.memory.read(var)

    def __write(self, var: Var, value):
        if isinstance(var, AliasVar):
            return self.__write(var.var, value)
        elif isinstance(var, PxiVar):
            dev = self.devices[var.slot]
            return dev.write(var, value)
        elif isinstance(var, HSLVar):
            dev = self.devices[var.pxi.slot]
            return dev.write(var, value)

    def __read(self, var: Var):
        if isinstance(var, AliasVar):
            return self.__read(var.var)
        elif isinstance(var, PxiVar):
            dev = self.devices[var.slot]
            value = dev.read(var)
            # self.memory.write(var, value)
            # return self.memory.read(var)
            return value
        elif isinstance(var, HSLVar):
            # TODO: HSL 没有支持远程读取
            return self.memory.read(var)


ioss = IOSS()
