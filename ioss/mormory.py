import array
import struct

from ioss.define import Var, ValType

BLOCK_SIZE = 1024
BUF_SIZE = 1024 * 16


class MemoryTable(object):
    def __init__(self):
        self.data = array.ArrayType('B', [0] * BUF_SIZE)

    def write(self, var: Var, value):
        offset = var.block.index * BLOCK_SIZE + var.block.offset
        if var.block.val_type == ValType.U64:
            struct.pack_into('<Q', self.data, offset, int(value))
        elif var.block.val_type == ValType.I64:
            struct.pack_into('<q', self.data, offset, int(value))
        elif var.block.val_type == ValType.U32:
            struct.pack_into('<L', self.data, offset, int(value))
        elif var.block.val_type == ValType.I32:
            struct.pack_into('<l', self.data, offset, int(value))
        elif var.block.val_type == ValType.U16:
            struct.pack_into('<H', self.data, offset, int(value))
        elif var.block.val_type == ValType.I16:
            struct.pack_into('<h', self.data, offset, int(value))
        elif var.block.val_type == ValType.U8:
            struct.pack_into('<B', self.data, offset, int(value))
        elif var.block.val_type == ValType.I8:
            struct.pack_into('<b', self.data, offset, int(value))
        elif var.block.val_type == ValType.D64:
            struct.pack_into('<d', self.data, offset, float(value))
        elif var.block.val_type == ValType.F32:
            struct.pack_into('<f', self.data, offset, float(value))
        elif var.block.val_type == ValType.BOOL:
            byte = struct.unpack_from('<B', self.data, offset)[0]
            try:
                flag = int(value)
                flag = 1 if flag > 0 else 0
            except ValueError:
                flag = 0

            op = 1 << var.block.bit
            if flag == 1:
                byte = byte | op
            if flag == 0:
                byte = byte & ~op
            struct.pack_into('<B', self.data, offset, byte)

    def read(self, var):
        offset = var.block.index * BLOCK_SIZE + var.block.offset
        if var.block.val_type == ValType.U64:
            return struct.unpack_from('<Q', self.data, offset)[0]
        elif var.block.val_type == ValType.I64:
            return struct.unpack_from('<q', self.data, offset)[0]
        elif var.block.val_type == ValType.U32:
            return struct.unpack_from('<L', self.data, offset)[0]
        elif var.block.val_type == ValType.I32:
            return struct.unpack_from('<l', self.data, offset)[0]
        elif var.block.val_type == ValType.U16:
            return struct.unpack_from('<H', self.data, offset)[0]
        elif var.block.val_type == ValType.I16:
            return struct.unpack_from('<h', self.data, offset)[0]
        elif var.block.val_type == ValType.U8:
            return struct.unpack_from('<B', self.data, offset)[0]
        elif var.block.val_type == ValType.I8:
            return struct.unpack_from('<b', self.data, offset)[0]
        elif var.block.val_type == ValType.D64:
            return struct.unpack_from('<d', self.data, offset)[0]
        elif var.block.val_type == ValType.F32:
            return struct.unpack_from('<f', self.data, offset)[0]
        elif var.block.val_type == ValType.BOOL:
            byte = struct.unpack_from('<B', self.data, offset)[0]
            byte = (byte >> var.block.bit) & 0x01
            return byte

    def dumps(self, path):
        """
        dump memory table to file
        :param path:
        :return:
        """
        self.data.tofile(path)
