import typing

import wx

from ioss.model import PxiSlotModel, PxiVarModel, HSLVarModel
from ioss.table import ioss


class IOssView(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        self.data = []
        self.__layout()

    def __layout(self):
        _art = wx.ArtProvider()
        self.lv = wx.ListView(self)
        self.lv.InsertColumn(0, '序号', width=40)
        self.lv.InsertColumn(1, 'SLOT', width=90)
        self.lv.InsertColumn(2, '地址', width=120)
        self.lv.InsertColumn(3, '描述', width=160)
        self.tb = wx.ToolBar(self, style=wx.TB_DEFAULT_STYLE | wx.TB_FLAT | wx.TB_VERTICAL | wx.TB_NODIVIDER)
        self.tb.AddTool(wx.ID_FILE1, 'ADD', _art.GetBitmap(wx.ART_PLUS))
        self.tb.AddTool(wx.ID_FILE2, 'DEL', _art.GetBitmap(wx.ART_MINUS))
        self.tb.AddSeparator()
        self.tb.AddStretchableSpace()
        self.tb.AddTool(wx.ID_FILE3, 'EDIT', _art.GetBitmap(wx.ART_HELP_SETTINGS))
        self.tb.AddTool(wx.ID_FILE4, 'DETAIL', _art.GetBitmap(wx.ART_LIST_VIEW))
        self.tb.Realize()
        msz = wx.BoxSizer(wx.HORIZONTAL)
        msz.Add(self.lv, 1, wx.EXPAND | wx.ALL, 10)
        msz.Add(self.tb, 0, wx.EXPAND | wx.ALL ^ wx.LEFT, 10)
        self.SetSizer(msz)
        self.Layout()

        self.tb.EnableTool(wx.ID_FILE1, False)
        self.tb.EnableTool(wx.ID_FILE2, False)
        self.tb.EnableTool(wx.ID_FILE3, False)
        self.tb.EnableTool(wx.ID_FILE4, False)
        # self.Bind(wx.EVT_TOOL, self.__on_edit, id=wx.ID_FILE3)
        self.Bind(wx.EVT_TOOL, self.__on_detail, id=wx.ID_FILE4)
        self.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.__on_detail, source=self.lv)
        self.Bind(wx.EVT_LIST_ITEM_SELECTED, self.__on_item_selected, source=self.lv)

    def clear(self):
        self.data.clear()
        self.lv.DeleteAllItems()

    def append(self, slot: PxiSlotModel):
        idx = self.lv.GetItemCount()
        self.lv.InsertItem(idx, f'{slot.id}')
        self.lv.SetItem(idx, 1, f'{slot.slot}')
        self.lv.SetItem(idx, 2, f'{slot.ip}:{slot.port}')
        self.lv.SetItem(idx, 3, f'{slot.desc}')

    # def __on_edit(self, evt):
    #     idx = self.lv.GetFirstSelected()
    #     slot = self.lv.GetItemText(idx, 1)
    #     dlg = wx.TextEntryDialog(self.GetTopLevelParent(), f'设置 {slot} IP 地址', '设置 Slot')
    #     if dlg.ShowModal() == wx.ID_OK:
    #         pass
    #     dlg.Destroy()
    #     evt.Skip()

    def __on_item_selected(self, evt):
        self.tb.EnableTool(wx.ID_FILE3, True)
        self.tb.EnableTool(wx.ID_FILE4, True)
        self.tb.Realize()
        evt.Skip()

    def __on_detail(self, evt):
        idx = self.lv.GetFirstSelected()
        slot = self.lv.GetItemText(idx, 1)
        dlg = wx.Dialog(self.GetTopLevelParent(), -1, f'{slot} 变量表', size=(600, 400))
        msz = wx.BoxSizer()
        vie = PxiVarView(dlg)
        vie.clear()
        if slot == 'PXI_HSL':
            for var in HSLVarModel.select():
                vie.append(var)
        else:
            for var in PxiVarModel.select().where(PxiVarModel.slot == slot):
                vie.append(var)
        msz.Add(vie, 1, wx.EXPAND | wx.ALL, 10)
        dlg.SetSizer(msz)
        dlg.Layout()
        dlg.Show()
        # dlg.ShowModal()
        # dlg.Destroy()
        evt.Skip()


class PxiVarView(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        self.data = []
        self.__layout()

    def __layout(self):
        self.lv = wx.ListView(self)
        self.lv.InsertColumn(0, '序号')
        self.lv.InsertColumn(1, '信号名')
        self.lv.InsertColumn(2, '通道')
        self.lv.InsertColumn(3, '类型')
        self.lv.InsertColumn(4, '值')
        self.lv.InsertColumn(5, '单位')

        msz = wx.BoxSizer()
        msz.Add(self.lv, 1, wx.EXPAND)
        self.SetSizer(msz)
        self.Layout()

        self.__timer = wx.Timer(self)

        self.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.__on_item_activated, source=self.lv)
        self.Bind(wx.EVT_TIMER, self.__on_timer, source=self.__timer)
        self.__timer.Start(500)

    def clear(self):
        self.data.clear()
        self.lv.DeleteAllItems()

    def append(self, var):
        idx = self.lv.GetItemCount()
        self.data.append(var)
        self.lv.InsertItem(idx, f'{var.id}')
        self.lv.SetItem(idx, 1, f'{var.sig_name}')
        self.lv.SetItem(idx, 2, f'{var.channel}')
        self.lv.SetItem(idx, 3, f'{var.sig_type}')
        self.lv.SetItem(idx, 5, f'{var.engineering_unit}')
        wx.YieldIfNeeded()

    def __on_item_activated(self, evt):
        idx = self.lv.GetFirstSelected()
        var = self.data[idx]
        dlg = wx.TextEntryDialog(self.GetTopLevelParent(), f'SET {var.sig_name} {var.channel} value')
        if dlg.ShowModal() == wx.ID_OK:
            ioss.write(var.sig_name, dlg.GetValue())
        dlg.Destroy()
        evt.Skip()

    def __on_timer(self, evt):
        rect = self.lv.GetViewRect()
        for idx in range(self.lv.GetItemCount()):
            pos = self.lv.GetItemPosition(idx)
            if 0 <= pos[1] <= rect[3]:
                var = self.data[idx]
                val = ioss.read(var.sig_name, remote=False)
                self.lv.SetItem(idx, 4, f'{val}')
        evt.Skip()

    def Destroy(self):
        self.__timer.Stop()
        super(PxiVarView, self).Destroy()

