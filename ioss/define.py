import typing
from dataclasses import dataclass
from enum import Enum

import modbus_tk.defines as cst


class ValType(Enum):
    BOOL = 0
    U8, U16, U32, U64 = 1, 2, 3, 4
    I8, I16, I32, I64 = 5, 6, 7, 8
    F32 = 9
    D64 = 10

    @classmethod
    def from_str(cls, s):
        return cls._member_map_.get(s)


@dataclass
class BlockInfo(object):
    index: int = 0
    offset: int = 0
    bit: int = 0
    val_type: ValType = ValType.U8


@dataclass
class Var(object):
    sig_name: typing.Text
    sig_type: typing.Text

    block: BlockInfo


@dataclass
class PxiVar(Var):
    sig_name: typing.Text
    sig_type: typing.Text
    slot: typing.Text
    channel: typing.Text
    engineering_unit: typing.Text
    rlo: float
    rhi: float
    elo: float
    ehi: float
    initial: float
    fcode: int
    address: int

    block: BlockInfo

    @classmethod
    def from_obj(cls, obj):
        reg = obj.reg
        if 10000 <= reg <= 19999:
            fcode = cst.COILS
            address = reg - fcode * 10000
        elif 20000 <= reg <= 29999:
            fcode = cst.DISCRETE_INPUTS
            address = reg - fcode * 10000
        elif 30000 <= reg <= 39999:
            fcode = cst.HOLDING_REGISTERS
            address = reg - fcode * 10000
        elif 40000 <= reg <= 49999:
            fcode = cst.ANALOG_INPUTS
            address = reg - fcode * 10000
        elif 100000 <= obj.reg <= 199999:
            fcode = cst.COILS
            address = reg - fcode * 100000
        elif 200000 <= reg <= 299999:
            fcode = cst.DISCRETE_INPUTS
            address = reg - fcode * 100000
        elif 300000 <= reg <= 399999:
            fcode = cst.HOLDING_REGISTERS
            address = reg - fcode * 100000
        elif 400000 <= reg <= 499999:
            fcode = cst.ANALOG_INPUTS
            address = reg - fcode * 100000
        else:
            raise VarNotFound(obj.sig_name)

        var = PxiVar(
            sig_name=obj.sig_name,
            sig_type=obj.sig_type,
            slot=obj.slot,
            channel=obj.channel,
            engineering_unit=obj.engineering_unit,
            rlo=obj.rlo,
            rhi=obj.rhi,
            elo=obj.elo,
            ehi=obj.ehi,
            initial=obj.initial,
            fcode=fcode,
            address=address,
            block=BlockInfo(
                index=obj.block,
                offset=obj.offset,
                bit=obj.bit,
                val_type=ValType.from_str(obj.chr)
            )
        )
        return var


@dataclass
class HSLVar(Var):
    sig_name: typing.Text
    sig_type: typing.Text
    port: int
    index: int
    data_type: typing.Text
    bit: typing.SupportsInt
    engineering_unit: typing.Text
    rlo: int
    rhi: int
    elo: int
    ehi: int
    initial: int

    pxi: PxiVar
    block: BlockInfo

    @classmethod
    def from_obj(cls, obj):
        from ioss.model import PxiVarModel
        p_obj = PxiVarModel \
            .filter(PxiVarModel.sig_type == 'sHSL') \
            .filter(PxiVarModel.sig_name == f'HSL_P{obj.port // 2 * 2}_{obj.index}') \
            .first()
        pxi = PxiVar.from_obj(p_obj)
        var = HSLVar(
            sig_name=obj.sig_name,
            sig_type=obj.sig_type,
            port=obj.port,
            index=obj.index,
            data_type=obj.data_type,
            bit=obj.bit,
            engineering_unit=obj.engineering_unit,
            rlo=obj.rlo,
            rhi=obj.rhi,
            elo=obj.elo,
            ehi=obj.ehi,
            initial=obj.initial,
            pxi=pxi,
            block=BlockInfo(
                index=pxi.block.index,
                offset=pxi.block.offset + obj.bit // 8,
                bit=obj.bit % 8,
                val_type=ValType.BOOL
            )
        )
        return var


@dataclass
class AliasVar(Var):
    sig_name: typing.Text
    sig_type: typing.Text

    block: BlockInfo

    desc: typing.Text
    var: typing.Optional[Var]


class VarNotFound(Exception):
    def __init__(self, sig_name):
        super(VarNotFound, self).__init__(f'Var {sig_name} Not Found')
