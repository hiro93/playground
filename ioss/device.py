import logging
import struct

import modbus_tk.defines as cst
from modbus_tk.modbus_tcp import TcpMaster

from ioss.define import PxiVar, ValType, HSLVar


class PxiMaster(TcpMaster):
    def __init__(self, host='127.0.0.1', port=502):
        TcpMaster.__init__(self, host, port)
        self.slave_id = 1
        self.__logger = logging.getLogger('ioss.pxi')

    def setup(self, obj):
        self._host = obj.ip
        self._port = int(obj.port)
        self.slave_id = int(obj.slave_id)

    def write(self, var: PxiVar, value):
        if var.fcode == cst.COILS:
            function_code = cst.WRITE_SINGLE_COIL
        elif var.fcode == cst.HOLDING_REGISTERS:
            function_code = cst.WRITE_MULTIPLE_REGISTERS
        else:
            raise IOError('不支持写的寄存器类')

        # TODO: 应用在三门 PMS 项目
        #  * 电流给百分比,
        #  * 电压给百分比,
        #  * 电阻输出 U=IR=0.000476*R 转换为百分比,
        #  * 频率输出 HZ
        if var.engineering_unit == 'amps':
            value = float(value)
            value = (value - var.rlo) / (var.rhi - var.rlo)
        elif var.engineering_unit == 'volts':
            value = float(value)
            value = (value - var.rlo) / (var.rhi - var.rlo)
        elif var.engineering_unit == 'OHMS':
            value = float(value) * 0.000476
            value = (value - var.elo) / (var.ehi - var.elo)
        elif var.sig_type == 'dPO':
            value = float(value)

        if var.block.val_type == ValType.BOOL:
            output_value = 1 if int(value) else 0
        elif var.block.val_type == ValType.D64:
            output_value = struct.unpack('<HHHH', struct.pack('<d', float(value)))
        elif var.block.val_type == ValType.U64:
            output_value = struct.unpack('<HHHH', struct.pack('<Q', int(value)))
        else:
            raise IOError('暂不支持的数据类型')

        self.__logger.info(f'WRITE {var.sig_name}={value}[{function_code},{var.address},{output_value}]')
        return self.execute(slave=self.slave_id,
                            function_code=function_code,
                            starting_address=var.address,
                            output_value=output_value)

    def read(self, var: PxiVar):
        if var.fcode in (cst.COILS, cst.DISCRETE_INPUTS, cst.HOLDING_REGISTERS, cst.ANALOG_INPUTS):
            function_code = var.fcode
        else:
            raise IOError('不支持的寄存器类')
        if var.block.val_type == ValType.BOOL:
            quantity_of_x = 1
        elif var.block.val_type == ValType.D64:
            quantity_of_x = 4
        elif var.block.val_type == ValType.U64:
            quantity_of_x = 4
        elif var.block.val_type == ValType.U32:
            quantity_of_x = 2
        else:
            raise IOError('暂不支持的数据类型')

        data = self.execute(
            slave=self.slave_id,
            function_code=function_code,
            starting_address=var.address,
            quantity_of_x=quantity_of_x
        )
        self.__logger.info(f'READ {var.sig_name}, [{function_code},{var.address}{data}]')

        if var.block.val_type == ValType.BOOL:
            return data[0]
        elif var.block.val_type == ValType.D64:
            return struct.unpack('<d', struct.pack('<HHHH', *data))[0]
        elif var.block.val_type == ValType.U64:
            return struct.unpack('<Q', struct.pack('<HHHH', *data))[0]
        elif var.block.val_type == ValType.U32:
            return struct.unpack('<L', struct.pack('<HH', *data))[0]
        else:
            raise IOError('暂不支持的数据类型')


class PxiHSL(PxiMaster):
    def __init__(self, *args, **kwargs):
        super(PxiHSL, self).__init__(*args, **kwargs)
        self.__logger = logging.getLogger('ioss.HSL')
        self.base_address = 6000
        self.beats = [2013000, 3013000, 4013000, 2023000, 3023000, 4023000]   # 0, 2, 4, 6, 8, 10
        self.memory = [0] * (1 + 32) * len(self.beats)
        for idx, sk in enumerate(self.beats):
            idx = idx * (1 + 32)
            self.memory[idx] = sk

    def setup(self, obj):
        super(PxiHSL, self).setup(obj)

    def set(self, port, index, bit, flag):
        try:
            flag = int(flag)
            flag = 1 if flag > 0 else 0
        except ValueError:
            flag = 0

        if 0 <= bit < 16:
            bit = bit + 16
        else:
            bit = bit - 16
        offset = port // 2 * (1 + 32) + 1 + index
        byte = self.memory[offset]
        op = 1
        assert 0 <= bit < 32
        op <<= bit
        if flag == 1:
            byte = byte | op
        if flag == 0:
            byte = byte & ~op
        self.memory[offset] = byte

    def write(self, var: HSLVar, value):
        self.set(var.port, var.index, var.bit, value)
        self.__logger.info(f'WRITE {var.sig_name}={value}')
        data = self.to_mb()
        for p in range(len(self.beats)):
            idx = p * (1 + 32) * 2
            self.execute(
                slave=self.slave_id,
                function_code=cst.WRITE_MULTIPLE_REGISTERS,
                starting_address=self.base_address + idx,
                output_value=data[idx:idx + 120]
            )

    def to_mb(self):
        data = struct.pack('<{}I'.format(len(self.memory)), *self.memory)
        value = struct.unpack('<{}H'.format(len(self.memory) * 2), data)
        return value
